import 'package:numbertrivia/features/trivia/data/models/trivia_model.dart';

abstract class RemoteDataSource {
  Future<TriviaModel> getRandomTrivia();
  Future<TriviaModel> getTrivia(int number);
}
