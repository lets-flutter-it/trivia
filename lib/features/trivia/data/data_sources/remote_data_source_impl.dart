import 'dart:convert';

import 'package:http/http.dart';
import 'package:numbertrivia/core/data/api.dart';
import 'package:numbertrivia/features/trivia/data/data_sources/remote_data_source.dart';
import 'package:numbertrivia/features/trivia/data/models/trivia_model.dart';

class RemoteDataSourceImpl implements RemoteDataSource {
  RemoteDataSourceImpl({Client client}) : client = client ?? Client();

  final Client client;

  static const Api api = ApiImpl('http://numbersapi.com');

  Future<TriviaModel> _fetchTrivia(String link) async {
    final response = await client.get(link);
    final json = jsonDecode(response.body) as Map<String, Object>;
    return TriviaModel.fromJson(json);
  }

  @override
  Future<TriviaModel> getRandomTrivia() =>
      _fetchTrivia(api.add('random').add('trivia?json').api);

  @override
  Future<TriviaModel> getTrivia(int number) =>
      _fetchTrivia(api.add('$number?json').api);
}
