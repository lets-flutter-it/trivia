import 'package:numbertrivia/features/trivia/data/data_sources/remote_data_source.dart';
import 'package:numbertrivia/features/trivia/data/data_sources/remote_data_source_impl.dart';
import 'package:numbertrivia/features/trivia/domain/entities/trivia.dart';
import 'package:numbertrivia/features/trivia/domain/repositories/trivia_repository.dart';

class TriviaRepositoryImpl implements TriviaRepository {
  TriviaRepositoryImpl({RemoteDataSource remoteDataSource})
      : remoteDataSource = remoteDataSource ?? RemoteDataSourceImpl();

  final RemoteDataSource remoteDataSource;

  @override
  Future<Trivia> getRandomTrivia() => remoteDataSource.getRandomTrivia();

  @override
  Future<Trivia> getTrivia(int number) => remoteDataSource.getTrivia(number);
}
