import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:numbertrivia/features/trivia/domain/entities/trivia.dart';

class TriviaModel extends Trivia with EquatableMixin {
  TriviaModel({
    @required String text,
    @required int number,
  }) : super(text: text, number: number);

  TriviaModel.fromJson(Map<String, Object> json)
      : super(
          text: json['text'],
          number: json['number'],
        );

  Map<String, Object> toJson() => {'text': text, 'number': number};

  @override
  List<Object> get props => [text, number];
}
