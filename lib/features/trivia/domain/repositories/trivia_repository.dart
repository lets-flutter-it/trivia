import 'package:numbertrivia/features/trivia/domain/entities/trivia.dart';

abstract class TriviaRepository {
  const TriviaRepository();

  Future<Trivia> getRandomTrivia();
  Future<Trivia> getTrivia(int number);
}
