import 'package:flutter/foundation.dart';

class Trivia {
  const Trivia({
    @required this.text,
    @required this.number,
  });

  final String text;
  final int number;
}
