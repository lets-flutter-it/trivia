import 'package:flutter/material.dart';
import 'package:numbertrivia/core/presentation/widgets/button.dart';
import 'package:numbertrivia/core/presentation/widgets/field.dart';
import 'package:numbertrivia/core/presentation/widgets/text_divider.dart';
import 'package:numbertrivia/features/trivia/application/trivia_provider.dart';
import 'package:provider/provider.dart';

class TriviaScreen extends StatelessWidget {
  const TriviaScreen();

  @override
  Widget build(BuildContext context) {
    final triviaProvider = TriviaProvider();
    return ChangeNotifierProvider(
      create: (_) => triviaProvider,
      child: Scaffold(
        appBar: AppBar(title: Text('Trivia App')),
        body: LayoutBuilder(
          builder: (context, constraints) {
            final height = constraints.biggest.height;
            return SingleChildScrollView(
              child: Center(
                child: Column(
                  children: [
                    SizedBox(height: height * .1),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Consumer<TriviaProvider>(
                        builder: (context, provider, _) => provider.loading
                            ? CircularProgressIndicator()
                            : Text(
                                provider.text,
                                textAlign: TextAlign.center,
                              ),
                      ),
                    ),
                    SizedBox(height: height * .1),
                    Field('Number', triviaProvider.controller),
                    Button(
                      'Search',
                      onPressed: () => triviaProvider.getTrivia(),
                    ),
                    SizedBox(height: height * .03),
                    TextDivider('OR'),
                    SizedBox(height: height * .03),
                    Button(
                      'Random',
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
