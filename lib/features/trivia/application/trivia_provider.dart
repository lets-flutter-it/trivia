import 'package:flutter/widgets.dart';
import 'package:numbertrivia/features/trivia/data/repositories/trivia_repository_impl.dart';
import 'package:numbertrivia/features/trivia/domain/repositories/trivia_repository.dart';

class TriviaProvider with ChangeNotifier {
  TriviaProvider({TriviaRepository triviaRepository})
      : triviaRepository = triviaRepository ?? TriviaRepositoryImpl();

  final controller = TextEditingController();

  final TriviaRepository triviaRepository;
  String _text = 'Enter a number...';
  bool _loading = false;

  String get text => _text;
  bool get loading => _loading;

  void getRandomTrivia() {}

  Future<void> getTrivia() async {
    _loading = true;
    notifyListeners();
    final number = int.tryParse(controller.text);

    if (number == null) {
      _text = 'ERROR';
      _loading = false;
      notifyListeners();
      return;
    }

    final trivia = await triviaRepository.getTrivia(number);
    _text = '${trivia.number}: ${trivia.text}';
    _loading = false;
    notifyListeners();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
