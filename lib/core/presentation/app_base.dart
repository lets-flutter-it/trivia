import 'package:flutter/material.dart';
import 'package:numbertrivia/features/trivia/presentation/trivia_screen.dart';

class AppBase extends StatelessWidget {
  const AppBase();

  @override
  Widget build(BuildContext context) => MaterialApp(
        home: TriviaScreen(),
      );
}
