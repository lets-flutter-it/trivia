import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button(
    this.label, {
    @required this.onPressed,
  });

  final String label;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) => RaisedButton(
        child: Text(
          label,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
          ),
        ),
        onPressed: onPressed,
        color: Theme.of(context).primaryColor,
      );
}
