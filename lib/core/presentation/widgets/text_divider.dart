import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TextDivider extends StatelessWidget {
  const TextDivider(this.text);

  final String text;

  @override
  Widget build(BuildContext context) =>
      LayoutBuilder(builder: (context, constraints) {
        final width = constraints.biggest.width;
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _Line(width * .2),
            SizedBox(width: width * .05),
            Text(text),
            SizedBox(width: width * .05),
            _Line(width * .2),
          ],
        );
      });
}

class _Line extends StatelessWidget {
  const _Line(this.width);

  final double width;

  @override
  Widget build(BuildContext context) => Container(
        color: Colors.black54,
        width: width,
        height: .5,
      );
}
