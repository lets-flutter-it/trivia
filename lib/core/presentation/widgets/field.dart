import 'package:flutter/material.dart';

class Field extends StatelessWidget {
  const Field(this.label, this.controller);

  final String label;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) => Container(
        width: 100,
        height: 100,
        child: TextField(
          controller: controller,
          decoration: InputDecoration(
            hintText: label,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        ),
      );
}
