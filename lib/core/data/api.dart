abstract class Api {
  const Api(this.api);
  final String api;
  Api add(String subLink);
}

class ApiImpl extends Api {
  const ApiImpl(String api) : super(api);

  Api add(String subLink) => ApiImpl('$api/$subLink');
}
