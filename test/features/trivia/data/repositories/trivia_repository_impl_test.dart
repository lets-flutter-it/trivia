import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:numbertrivia/features/trivia/data/data_sources/remote_data_source.dart';
import 'package:numbertrivia/features/trivia/data/models/trivia_model.dart';
import 'package:numbertrivia/features/trivia/data/repositories/trivia_repository_impl.dart';
import 'package:numbertrivia/features/trivia/domain/repositories/trivia_repository.dart';

class MockRemoteDataSource extends Mock implements RemoteDataSource {}

void main() {
  TriviaRepository triviaRepository;
  MockRemoteDataSource remoteDataSource;

  setUp(() {
    remoteDataSource = MockRemoteDataSource();
    triviaRepository = TriviaRepositoryImpl(remoteDataSource: remoteDataSource);
  });

  group('getTrivia', () {
    test(
      'should call remoteDataSource when the method is called',
      () async {
        // arrange
        final tNumber = 5;
        final tTrivia = TriviaModel(number: tNumber, text: 'test');
        when(remoteDataSource.getTrivia(tNumber)).thenAnswer(
          (_) async => tTrivia,
        );
        // act
        final result = await triviaRepository.getTrivia(tNumber);
        // assert
        expect(result, tTrivia);
        verify(remoteDataSource.getTrivia(tNumber)).called(1);
        verifyNever(remoteDataSource.getRandomTrivia());
      },
    );
  });
}
