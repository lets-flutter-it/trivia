import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:numbertrivia/features/trivia/data/data_sources/remote_data_source.dart';
import 'package:numbertrivia/features/trivia/data/data_sources/remote_data_source_impl.dart';
import 'package:numbertrivia/features/trivia/data/models/trivia_model.dart';

class MockClient extends Mock implements Client {}

void main() {
  RemoteDataSource remoteDataSource;
  MockClient client;

  setUp(() {
    client = MockClient();
    remoteDataSource = RemoteDataSourceImpl(client: client);
  });

  group('getRandomTrivia', () {
    test(
      'should return the corresponding TriviaModel according to the returned data',
      () async {
        // arrange
        final tTrivia = TriviaModel(text: 'test', number: 11);

        final data = jsonEncode(tTrivia.toJson());

        when(client.get(any)).thenAnswer((_) async => Response(data, 200));

        // act
        final trivia = await remoteDataSource.getRandomTrivia();

        // assert
        expect(trivia, tTrivia);
      },
    );
  });

  group('getTrivia', () {
    test(
      'should return the corresponding TriviaModel according to the returned data',
      () async {
        // arrange
        final tNumber = 11;
        final tTrivia = TriviaModel(text: 'test', number: tNumber);

        final data = jsonEncode(tTrivia.toJson());

        when(client.get(contains(tNumber.toString())))
            .thenAnswer((_) async => Response(data, 200));

        // act
        final trivia = await remoteDataSource.getTrivia(tNumber);

        // assert
        expect(trivia, tTrivia);
      },
    );
  });
}
